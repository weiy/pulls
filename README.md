# Pulls

The tutorial gives an example to illustrate the properties of the pulls. In this
example, the author focuses on a scenario involving only one process,
one channel or category, and one bin for the sake of simplicity.
(However, it can be easily extended to scenarios with multiple
processes, channels and bins.) In this single channel, the data exhibits
120 events and the expectation is 100. Three types of systematics are
considered, as shown in Table **1.1** and
Figure **1.1**, including Group I, II, and III for
symmetric, flipped, and one-sided systematics, respectively.


!\[image info\](systematic_table.png){ width=600px }


!\[image info\](pull_full_with_title.png){width=600px}


From these three groups, three key properties of pulls can be deduced.

**Property 1**: The fitted $\sigma$ in pulls reflects the constrained
systematics, a conclusion drawn from Group I systematics.
Figure **1.2**  shows the trends of fitted $\mu$ and
$\sigma$ in pulls as functions of variation. Alternatively, given known
pulls (as shown in Figure **1.1**, the variation can be estimated. This
provides a method to verify the variation in a fit.

**Property 2**: For flipped variations, the fitted $\mu$ exhibits an
opposite sign, a result derived from Group II systematics, while the
fitted $\sigma$ remains unchanged. This behaviour is intuitive.

**Property 3**: A fit can become divergent if the systematics are
one-sided and the fitted $\sigma$ is greater than 1. The corresponding
fitted $\mu$ and $\sigma$ are shown in
Figure **1.3** as functions of variations.
In most analyses, the Hessian method in RooFit is used for fitting.
However, one-sided systematics, which can stem from MC simulation
fluctuations, acceptance changes, or other reasons, may not perform well
in this method. To address this, in the off-shell Higgs analysis, the
systematics are systematized and smoothed to avert fit divergence.

!\[image info\](fitted_sigma_vs_var_final_with_title.png){width=600px}

!\[image info\](fitted_sigma_vs_var_final_nonconverge_updownsame_with_title.png){width=600px}

While the above discussion numerically presents pull properties, a
deeper analytical explanation directly from the mathematical definition
of pulls necessitates further exploration.

The code used to generate the pull results and fitted sigma vs variations is provided in python code.

