import json

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import pyhf

pull_weiy = [0.000, 0.380, 0.782, 0.976, 0.395, 0.204, 1000 ,0.782, -0.782, 1000, 0.000, 0.000, 0.000, -0.342, -0.436, -0.690]
pullerr_weiy = [0.993, 0.971 , 0.900, 0.708, 0.233, 0.125, 0, 0.900, 0.900, 0, 0.993, 1.251, 1.715, 1.632, 1.274, 0.787]
label_weiy = np.array(["[up down = 1.00 1.00]",
                       "[up down = 1.02 0.98]",
                       "A [up down = 1.05 0.95]", 
                       "B [up down = 1.10 0.90]", 
                       "C [up down = 1.50 0.50]", 
                       "D [up down = 1.90 0.10]",
                       "",
                       "A [up down = 1.05 0.95]", 
                       "A_flip [up down = 0.95 1.05]",
                       "",
                       "[up down = 1.00 1.00]",
                       "E [up down = 1.005 1.005]",
                       "F [up down = 1.009 1.009]",
                       "G [up down = 1.016 1.016]",
                       "H [up down = 1.018 1.018]",
                       "[up down = 1.03 1.03]",
                        ])

fig, ax = plt.subplots()
fig.set_size_inches(20, 8)

# set up axes labeling, ranges, etc...
ax.tick_params(axis='both', which='major', labelsize=25)
ax.xaxis.set_major_locator(mticker.FixedLocator(np.arange(label_weiy.size).tolist()))
ax.set_xticklabels(label_weiy, rotation=50, ha="right",fontsize=20)
ax.set_xlim(-0.5, len(pull_weiy) - 0.5)
ax.set_ylim(-2, 2)
ax.set_title("Pull Plot", fontsize=25)
ax.set_ylabel(r"$(\theta - \hat{\theta})\,/ \Delta \theta$", fontsize=25)

# draw the +/- 2.0 horizontal lines
ax.hlines([-2, 2], -0.5, len(pull_weiy) - 0.5, colors="black", linestyles="dotted")
# draw the +/- 1.0 horizontal lines
ax.hlines([-1, 1], -0.5, len(pull_weiy) - 0.5, colors="black", linestyles="dashdot")
# draw the +/- 2.0 sigma band
ax.fill_between([-0.5, len(pull_weiy) - 0.5], [-2, -2], [2, 2], facecolor="yellow")
# drawe the +/- 1.0 sigma band
ax.fill_between([-0.5, len(pull_weiy) - 0.5], [-1, -1], [1, 1], facecolor="green")
# draw a horizontal line at pull=0.0
ax.hlines([0], -0.5, len(pull_weiy) - 0.5, colors="black", linestyles="dashed")
# finally draw the pulls
ax.scatter(range(len(pull_weiy)), pull_weiy, color="black")
# and their uncertainties
ax.errorbar(
    range(len(pull_weiy)),
    pull_weiy,
    color="black",
    xerr=0,
    yerr=pullerr_weiy,
    marker=".",
    fmt="none",
)

# error > 1
# error_gt1 = np.argmax(errors > 1) - 0.5
# ax.axvline(x=error_gt1, color="red", linestyle="--")
# ax.text(
#    error_gt1 + 0.1, 1.5, r"$\sigma \geq 1 \longrightarrow$", color="red", fontsize=25
# );
plt.subplots_adjust(right=0.93,bottom=0.4)
fig.savefig("pull_full.png")
