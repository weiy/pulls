import matplotlib.pyplot as plt

# Sample data
# data is 80 events
# expectation is 100 events

#original sigma

up =           [  0.2,   0.5,   0.6,   0.8,   0.9,  0.95,  0.98,   1.0]
var =          [1-i for i in up]
#var =         [  0.8,   0.5,   0.4,  0.20,  0.10,  0.05,  0.02,  0.00]
#var = original sigma

fitted_mu =    [0.206, 0.355, 0.445, 0.807, 1.027, 0.818, 0.389, 0.000]
fitted_sigma = [0.089, 0.157, 0.204, 0.428, 0.705, 0.879, 0.977, 0.993]

# Create a plot
# plt.plot(up, fitted_mu, marker='o')
# plt.plot(var, fitted_mu, marker='o')
plt.plot(var, fitted_sigma, marker='o')

# Adding labels and title

# plt.xlabel('up')
# plt.ylabel('fitted_mu')
# plt.title('fitted_mu vs up')

# plt.xlabel('var')
# plt.ylabel('fitted_mu')
# plt.title('fitted_mu vs var')

plt.xlabel('var')
plt.ylabel('fitted_sigma')
plt.title('fitted_sigma vs var')

# Display the plot
plt.savefig("fitted_sigma_vs_var.png")
plt.show()
