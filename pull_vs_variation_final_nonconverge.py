import numpy as np
import matplotlib.pyplot as plt

# Read data from the text file
filename = 'fitted_vs_var_nonconverge_updownsame.txt'  # Replace with the actual path to your text file

variations = []
fitted_mu = []
fitted_sigma = []

with open(filename, 'r') as file:
    lines = file.readlines()
    for line in lines:
        if not line.startswith('#'):
            parts = line.strip().split(',')
            var = float(parts[1].split()[1].replace("'",""))
            print(parts[-1].split(','))
            print(parts[-2].split(','))
            fitted_val_mu = float(parts[-2].split(',')[0].replace("]","").replace("[",""))
            fitted_val_sigma = float(parts[-1].split(',')[0].replace("]","").replace("[",""))
            variations.append(var)
            fitted_mu.append(fitted_val_mu)
            fitted_sigma.append(fitted_val_sigma)
            
variations = [i-1 for i in variations]
# Create and display the plot
# plt.figure(figsize=(10, 6))
# plt.plot(variations, fitted_mu, marker='o', linestyle='-', color='b')
# plt.plot(variations, fitted_sigma, marker='o', linestyle='-', color='y')
# plt.xlabel('Variation')
# plt.ylabel('Fitted mu')
# plt.title('Variation vs Fitted mu')
# plt.grid(True)
# plt.savefig("fitted_sigma_vs_var_final.png")
# plt.show()




fig, ax1 = plt.subplots(figsize=(9, 6))

myfontsize=20

ax1.plot(variations, fitted_mu, linestyle='-', marker='o', color='b', label=r'Fitted $\mu$')
ax1.set_xlabel('Variation',fontsize=myfontsize)
ax1.set_ylabel(r'Fitted $\mu$', color='b',fontsize=myfontsize)
ax1.tick_params(axis='y', labelcolor='b')
ax1.grid(True)
# ax1.legend(loc='upper right',fontsize=myfontsize)

ax2 = ax1.twinx()  # Create a secondary y-axis
ax2.plot(variations, fitted_sigma, marker='s', linestyle='-', color='r', label=r'Fitted $\sigma$')
ax2.set_ylabel(r'Fitted $\sigma$', color='r',fontsize=myfontsize)
ax2.tick_params(axis='y', labelcolor='r')
# ax2.legend(loc='upper right',fontsize=myfontsize)

lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc='upper right', fontsize=myfontsize)


ax1.tick_params(axis='both', which='major', labelsize=myfontsize)
ax2.tick_params(axis='both', which='major', labelsize=myfontsize)

plt.subplots_adjust(right=0.86,bottom=0.13)

ax1.set_ylim(-1.05,8)
ax2.set_ylim(-1.05,8)

plt.title(r'Variation vs Fitted $\mu$ and Fitted $\sigma$ in pull',fontsize=myfontsize)
plt.savefig("fitted_sigma_vs_var_final_nonconverge_updownsame.png")
